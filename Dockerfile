FROM mcr.microsoft.com/dotnet/sdk:6.0
RUN ls -la

RUN apt-get update
RUN apt-get install -y ca-certificates curl
RUN apt-get install -y git libnss3-tools
RUN git clone https://gitlab.com/OksanaPod31/cource.archive.git

WORKDIR /cource.archive
RUN ls -la

WORKDIR src/cource.archive
RUN dotnet dev-certs https --trust
RUN dotnet dev-certs https -ep /usr/local/share/ca-certificates/aspnet/https.crt --format PEM

RUN dotnet restore
RUN dotnet publish -c Release -o cource.archive/bin/Release/net6.0/publish

WORKDIR /cource.archive/src/cource.archive/cource.archive/bin/Release/net6.0/publish
ENTRYPOINT ["dotnet", "cource.archive.dll", "--environment=Production"]
