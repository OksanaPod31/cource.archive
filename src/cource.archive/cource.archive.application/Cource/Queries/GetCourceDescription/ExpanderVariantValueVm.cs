using AutoMapper;
using cource.archive.application.Mapping;
using cource.archive.domain;

namespace cource.archive.application.Cource.Queries.GetCourceDescription;

public class ExpanderVariantValueVm : IMapWith<ExpanderVariantValue>
{
    public string Id { get; set; }
    public string Value { get; set; }
    
    public void Mapping(Profile profile)
    {
        profile.CreateMap<ExpanderVariantValue, ExpanderVariantValueVm>()
            .ForMember(vm => vm.Id,
                opt => opt.MapFrom(cor => cor.Id.ToString()))
            .ForMember(vm => vm.Value,
                opt => opt.MapFrom(cor => cor.Value));
    }
}