using AutoMapper;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.application.Mapping;
using cource.archive.domain;

namespace cource.archive.application.Cource.Queries.GetCourceDescription;

public class ExpanderVm 
{
    public string ExpanderName { get; set; }
    public IList<ExpanderValueVm> ExpanderValues { get; set; }
}