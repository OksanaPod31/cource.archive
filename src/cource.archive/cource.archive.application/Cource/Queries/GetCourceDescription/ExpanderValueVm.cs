namespace cource.archive.application.Cource.Queries.GetCourceDescription;

public class ExpanderValueVm
{
    public string Id { get; set; }
    public string Name { get; set; }
    public IList<ExpanderVariantValueVm> Values { get; set; }
}