using MediatR;

namespace cource.archive.application.Cource.Queries.GetCourceDescription;

public class GetCourceDescriptionQuery: IRequest<IList<ExpanderVm>>
{
    public string CourceId { get; set; }
}