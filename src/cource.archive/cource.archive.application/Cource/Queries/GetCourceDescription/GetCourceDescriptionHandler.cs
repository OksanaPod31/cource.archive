using AutoMapper;
using cource.archive.application.Constants;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Queries.GetCourceDescription;

public class GetCourceDescriptionHandler: IRequestHandler<GetCourceDescriptionQuery, IList<ExpanderVm>>
{
    private readonly IMongoCollection<CourceSpecification> _specCollection;
    private readonly IMongoCollection<ExpanderVariant> _expanderCollection;
    private readonly IMongoCollection<ExpanderVariantValue> _expanderValueCollection;
    private readonly IMapper _mapper;

    public GetCourceDescriptionHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _specCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<CourceSpecification>(settings.CourceSpecification);
        _expanderCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<ExpanderVariant>(settings.ExpanderVariant);
        _expanderValueCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<ExpanderVariantValue>(settings.ExpanderVariantValue);
        _mapper = mapper;
    }
    
    public async Task<IList<ExpanderVm>> Handle(GetCourceDescriptionQuery request, CancellationToken cancellationToken)
    {
        var spec = await _specCollection.Find(sp =>
                sp.CourceId == ObjectId.Parse(request.CourceId) &&
                sp.SpecificationId == SpecificationConstants.Id.Expander)
            .ToListAsync(cancellationToken);
        var expanders = new List<ExpanderVm>();

        var expandersGroup = spec.GroupBy(sc => sc.StringValue);
        foreach (var group in expandersGroup)
        {
            var expanderVm = new ExpanderVm();
            expanderVm.ExpanderName = group.Key ?? "";
            expanderVm.ExpanderValues = new List<ExpanderValueVm>();
            foreach (var sp in group)
            {
                var expanderVariants = await _expanderCollection.Find(ex => ex.Id == sp.LookupValue).ToListAsync(cancellationToken);
                foreach (var expanderVariant in expanderVariants)
                {
                    var expanderVariantValueVm = new List<ExpanderVariantValueVm>();
                    var expanderVariantValues = await _expanderValueCollection
                        .Find(exv => exv.ExpanderVariant == expanderVariant.Id).ToListAsync(cancellationToken);
                    expanderVariantValues.ForEach(elem =>
                    {
                        expanderVariantValueVm.Add(_mapper.Map<ExpanderVariantValueVm>(elem));
                    });

                    expanderVm.ExpanderValues.Add(new ExpanderValueVm
                    {
                        Id = expanderVariant.Id.ToString(),
                        Name = expanderVariant.StringValue,
                        Values = expanderVariantValueVm
                    });
                }
            }
            expanders.Add(expanderVm);
        }
            
        return expanders;
    }
}