using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using MediatR;

namespace cource.archive.application.Cource.Queries.GetAvailableCourcesNotIncludingTopic;

public class GetAvailableCourcesNotIncludingTopicQuery : IRequest<IList<CourceVm>>
{
    public string UserId { get; set; }
    public string[] RoomsId { get; set; }
    public string TopicId { get; set; }
}