using cource.archive.application.Cource.Queries.GetChapterById;
using cource.archive.domain;
using MediatR;

namespace cource.archive.application.Cource.Queries.GetChapterByCourceId;

public class GetChaptersByCourceIdQuery : IRequest<IList<ChapterVm>>
{
    public string CourceId { get; set; }
}