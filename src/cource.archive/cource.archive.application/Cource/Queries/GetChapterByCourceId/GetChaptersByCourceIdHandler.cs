using cource.archive.application.Cource.Queries.GetChapterById;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Driver;
using IMapper = AutoMapper.IMapper;

namespace cource.archive.application.Cource.Queries.GetChapterByCourceId;

public class GetChaptersByCourceIdHandler : IRequestHandler<GetChaptersByCourceIdQuery, IList<ChapterVm>>
{
    private readonly IMongoCollection<Chapter> _collection;
    private readonly IMapper _mapper;

    public GetChaptersByCourceIdHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<Chapter>(settings.ChapterCollectionName);
        _mapper = mapper;
    }
    
    public async Task<IList<ChapterVm>> Handle(GetChaptersByCourceIdQuery request, CancellationToken cancellationToken)
    {
        var result = new List<ChapterVm>();
        var collection = await _collection.Find(c => c.CourceId == request.CourceId).ToListAsync();
        foreach (var chapter in collection)
        {
            result.Add(_mapper.Map<ChapterVm>(chapter));
        }
        return result;
    }
}