using AutoMapper;
using cource.archive.application.Cource.Queries.GetChapterById;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.application.Cource.Queries.GetTopicList;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Queries.GetWorks;

public class GetWorksHandler : IRequestHandler<GetWorksQuery, WorksVm>
{
    private readonly IMongoCollection<Topic> _topicCollection;
    private readonly IMongoCollection<domain.Cource> _courceCollection;
    private readonly IMongoCollection<Chapter> _chapterCollection;
    private readonly IMapper _mapper;

    public GetWorksHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _topicCollection = client.GetDatabase(settings.DataBaseName).GetCollection<Topic>(settings.TopicCollection);
        _courceCollection = client.GetDatabase(settings.DataBaseName).GetCollection<domain.Cource>(settings.CourceCollectionName);
        _chapterCollection = client.GetDatabase(settings.DataBaseName).GetCollection<Chapter>(settings.ChapterCollectionName);
        _mapper = mapper;
    }
    
    public async Task<WorksVm> Handle(GetWorksQuery request, CancellationToken cancellationToken)
    {
        var topicsList = await _topicCollection.Find(t => t.CreatedBy == request.UserId).ToListAsync(cancellationToken);
        var courcesByCreation = await _courceCollection.Find(c => c.CreatedBy == request.UserId).ToListAsync(cancellationToken) ?? new List<domain.Cource>();
        var cources = new List<CourceVm>();
        var chaptersList = new List<Chapter>();
        if (courcesByCreation is not null)
        {
            courcesByCreation.ForEach(cource =>
            {
                cources.Add(_mapper.Map<CourceVm>(cource));
            });
        }
        foreach (var roomId in request.RoomsId)
        {
            var cource = await _courceCollection.Find(c => c.RoomId == roomId).FirstOrDefaultAsync(cancellationToken);
            if (cource is not null)
            {
                if(cources.Count(c => c.Id == cource.Id.ToString()) <= 0)
                {
                    cources.Add(_mapper.Map<CourceVm>(cource));
                }
            }
        }
        
        foreach (var courceVm in cources)
        {
            var chapterByCource = await _chapterCollection.Find(c => c.CourceId == courceVm.Id)
                .ToListAsync(cancellationToken);
            if (chapterByCource?.Count > 0)
            {
                chaptersList.AddRange(chapterByCource);
            }
        }

        var chaptersListByCreation = await _chapterCollection.Find(ch => ch.CreatedBy == request.UserId)
            .ToListAsync(cancellationToken);
        var topics = new List<TopicVm>();
        var chapters = new List<ChapterVm>();

        if (topicsList is not null)
        {
            topicsList.ForEach(topic =>
            {
                topics.Add(_mapper.Map<TopicVm>(topic));
            });
        }

        chaptersList.ForEach(chapter =>
        {
            chapters.Add(_mapper.Map<ChapterVm>(chapter));
        });
        if (chaptersListByCreation is not null)
        {
            chaptersListByCreation.ForEach(chapter =>
            {
                chapters.Add(_mapper.Map<ChapterVm>(chapter));
            });
        }

        return new WorksVm
        {
            Chapters = chapters,
            Cources = cources,
            Topics = topics
        };
    }
}