using MediatR;

namespace cource.archive.application.Cource.Queries.GetWorks;

public class GetWorksQuery: IRequest<WorksVm>
{
    public string UserId { get; set; }
    public IList<string> RoomsId { get; set; }
}