using cource.archive.application.Cource.Queries.GetChapterById;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.application.Cource.Queries.GetTopicList;

namespace cource.archive.application.Cource.Queries.GetWorks;

public class WorksVm
{
    public IList<CourceVm> Cources { get; set; }
    public IList<TopicVm> Topics { get; set; }
    public IList<ChapterVm> Chapters { get; set; }
}