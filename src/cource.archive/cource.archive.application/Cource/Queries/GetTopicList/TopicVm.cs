using AutoMapper;
using cource.archive.application.Mapping;
using cource.archive.domain;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace cource.archive.application.Cource.Queries.GetTopicList;

public class TopicVm : IMapWith<Topic>
{
    
    public string Id { get; set; }
    public DateTime? CreatedOn { get; set; }
    public string? CreatedBy { get; set; }
    public string? Name { get; set; }
    public string TitleId { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<Topic, TopicVm>()
            .ForMember(vm => vm.Id,
                opt => opt.MapFrom(top => top.Id.ToString()))
            .ForMember(vm => vm.Name,
                opt => opt.MapFrom(top => top.Name))
            .ForMember(vm => vm.CreatedBy,
                opt => opt.MapFrom(top => top.CreatedBy))
            .ForMember(vm => vm.CreatedOn,
                opt => opt.MapFrom(top => top.CreatedOn))
            .ForMember(vm => vm.TitleId,
                opt => opt.MapFrom(top => top.TitleId.ToString()));
    }
}