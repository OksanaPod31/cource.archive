using AutoMapper;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Queries.GetTopicList;

public class GetTopicListHandler : IRequestHandler<GetTopicListQuery, IList<TopicVm>>
{
    private readonly IMongoCollection<domain.Topic> _collection;
    private readonly IMapper _mapper;

    public GetTopicListHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<Topic>(settings.TopicCollection);
        _mapper = mapper;
    }
    public async Task<IList<TopicVm>> Handle(GetTopicListQuery request, CancellationToken cancellationToken)
    {
        var topics =  await _collection.Find(top => true).ToListAsync();
        var mapList = new List<TopicVm>();

        if (topics is not null)
        {
            foreach (var topic in topics)
            {
                /*var stream = new MemoryStream();
                await _gridFsBucket.DownloadToStreamAsync(topic.TitleId, stream);//await _gridFsBucket.DownloadAsBytesAsync(topic.TitleId);;
                topic.TitleFile = stream;
                //stream.CopyTo(topic.TitleFile);
                topic.TitleFile.Position = 0;*/
                mapList.Add(_mapper.Map<TopicVm>(topic));
                
            }
        }

        return mapList;
    }
}