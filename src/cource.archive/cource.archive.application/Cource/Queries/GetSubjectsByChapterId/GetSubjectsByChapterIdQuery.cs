using cource.archive.domain;
using MediatR;

namespace cource.archive.application.Cource.Queries.GetSubjectsByChapterId;

public class GetSubjectsByChapterIdQuery : IRequest<IList<SubjectVm>>
{
    public string ChapterId { get; set; }
}