using AutoMapper;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Queries.GetSubjectsByChapterId;

public class GetSubjectsByChapterIdHandler : IRequestHandler<GetSubjectsByChapterIdQuery, IList<SubjectVm>>
{
    private readonly IMongoCollection<Subject> _collection;
    private readonly IMapper _mapper;

    public GetSubjectsByChapterIdHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<Subject>(settings.Subject);
        _mapper = mapper;
    }
    
    public async Task<IList<SubjectVm>> Handle(GetSubjectsByChapterIdQuery request, CancellationToken cancellationToken)
    {
        var subjects = await _collection.Find(sb => sb.ChapterId == ObjectId.Parse(request.ChapterId)).ToListAsync(cancellationToken);
        var result = new List<SubjectVm>();
        subjects.ForEach(elem => result.Add(_mapper.Map<SubjectVm>(elem)));
        return result;
    }
}