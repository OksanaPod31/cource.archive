using AutoMapper;
using cource.archive.application.Mapping;
using cource.archive.domain;

namespace cource.archive.application.Cource.Queries.GetSubjectsByChapterId;

public class SubjectVm : IMapWith<Subject>
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string FileId { get; set; }
    
    public void Mapping(Profile profile)
    {
        profile.CreateMap<Subject, SubjectVm>()
            .ForMember(vm => vm.Id,
                opt => opt.MapFrom(top => top.Id.ToString()))
            .ForMember(vm => vm.Name,
                opt => opt.MapFrom(top => top.Name))
            .ForMember(vm => vm.FileId, opt => opt.MapFrom(sub => sub.File));
    }
}