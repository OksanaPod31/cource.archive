using AutoMapper;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Queries.GetCourceList;

public class GetCourceListHandler : IRequestHandler<GetCourceListQuery, IList<CourceVm>>
{
    private readonly IMongoCollection<domain.Cource> _collection;
    private readonly IMapper _mapper;

    public GetCourceListHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<domain.Cource>(settings.CourceCollectionName);
        _mapper = mapper;
    }
    public async Task<IList<CourceVm>> Handle(GetCourceListQuery request, CancellationToken cancellationToken)
    {
        var result = new List<CourceVm>();
        var cources = await _collection.Find(cource => true).ToListAsync();
        cources.ForEach(elem =>
        {
            result.Add(_mapper.Map<CourceVm>(elem));
        });

        return result;
    }
}