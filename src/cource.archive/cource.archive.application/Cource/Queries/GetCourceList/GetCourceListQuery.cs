using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using MediatR;

namespace cource.archive.application.Cource.Queries.GetCourceList;

public class GetCourceListQuery : IRequest<IList<CourceVm>>
{
    
}