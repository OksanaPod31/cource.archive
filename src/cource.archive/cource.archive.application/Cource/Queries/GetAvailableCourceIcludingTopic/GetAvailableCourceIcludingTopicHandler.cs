using AutoMapper;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Queries.GetAvailableCourceIcludingTopic;

public class GetAvailableCourceIcludingTopicHandler : IRequestHandler<GetAvailableCourceIcludingTopicQuery, IList<CourceVm>>
{
    private readonly IMongoCollection<CourceSpecification> _courceSpecification;
    private readonly IMongoCollection<domain.Cource> _courceCollection;
    private readonly IMapper _mapper;
    
    public GetAvailableCourceIcludingTopicHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _courceSpecification = client.GetDatabase(settings.DataBaseName)
            .GetCollection<CourceSpecification>(settings.CourceSpecification);
        _courceCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<domain.Cource>(settings.CourceCollectionName);
        _mapper = mapper;
    }
    
    public async Task<IList<CourceVm>> Handle(GetAvailableCourceIcludingTopicQuery request, CancellationToken cancellationToken)
    {
        var courcesByCreation = await _courceCollection.Find(c => c.CreatedBy == request.UserId).ToListAsync(cancellationToken) ?? new List<domain.Cource>();
        var cources = new List<CourceVm>();

        var courceSpec = await _courceSpecification.Find(s => s.LookupValue == ObjectId.Parse(request.TopicId)).ToListAsync(cancellationToken);
        
        if (courcesByCreation is not null)
        {
            courcesByCreation.ForEach(cource =>
            {
                if (courceSpec.Count(c => c.CourceId == cource.Id) > 0)
                {
                    cources.Add(_mapper.Map<CourceVm>(cource));
                }
            });
        }
        foreach (var roomId in request.RoomsId)
        {
            var cource = await _courceCollection.Find(c => c.RoomId == roomId).FirstOrDefaultAsync(cancellationToken);
            if (cource is not null)
            {
                if (courceSpec.Count(c => c.CourceId == cource.Id) > 0 && cources.Count(c => c.Id == cource.Id.ToString()) <= 0)
                {
                    cources.Add(_mapper.Map<CourceVm>(cource));
                }
            }
        }

        return cources;
    }
}