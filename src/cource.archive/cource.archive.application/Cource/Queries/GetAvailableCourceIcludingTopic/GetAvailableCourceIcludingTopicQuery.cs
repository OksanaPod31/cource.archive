using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using MediatR;

namespace cource.archive.application.Cource.Queries.GetAvailableCourceIcludingTopic;

public class GetAvailableCourceIcludingTopicQuery : IRequest<IList<CourceVm>>
{
    public string UserId { get; set; }
    public string[] RoomsId { get; set; }
    public string TopicId { get; set; }
}