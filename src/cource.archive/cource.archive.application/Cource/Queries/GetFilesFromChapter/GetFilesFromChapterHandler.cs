using System.IO.Compression;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using Microsoft.AspNetCore.Hosting;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Queries.GetFilesFromChapter;

public class GetFilesFromChapterHandler : IRequestHandler<GetFilesFromChapterQuery, MemoryStream>
{
    private readonly IGridFSBucket _gridFsBucket;
   
 
    public GetFilesFromChapterHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
        
    }

    public async Task<MemoryStream> Handle(GetFilesFromChapterQuery request, CancellationToken cancellationToken)
    {
        var memoryStream = new MemoryStream();
        using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
        {
            foreach (var fileName in request.FileNames)
            {
                var byteArray = await _gridFsBucket.DownloadAsBytesByNameAsync(fileName);
                ZipArchiveEntry entry = zipArchive.CreateEntry(fileName);
                
                using (StreamWriter writer = new StreamWriter(entry.Open()))
                {
                    writer.Write(byteArray);
                }
            }
            
        }
        memoryStream.Position = 0;
        return memoryStream;
        //throw new NotImplementedException();
        /*var fs = new FileStream(Path.Combine(_basePath, "archive.zip"), FileMode.OpenOrCreate);

        using (var memoryStream = new MemoryStream())
        {
            using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
            {
                foreach (var file in request.FileNames)
                {
                    zipArchive.CreateEntryFromFile(Path.Combine(_basePath, file), file);
                }
            }

            memoryStream.Position = 0;
            fs.Position = 0;
            memoryStream.WriteTo(fs);
        }
        foreach (var fileName in request.FileNames)
        {
            _gridFsBucket.DownloadToStreamByName(fileName);
        }*/

    }
}