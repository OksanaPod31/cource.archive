using MediatR;

namespace cource.archive.application.Cource.Queries.GetFilesFromChapter;

public class GetFilesFromChapterQuery : IRequest<MemoryStream>
{
    public List<string> FileNames { get; set; }
}