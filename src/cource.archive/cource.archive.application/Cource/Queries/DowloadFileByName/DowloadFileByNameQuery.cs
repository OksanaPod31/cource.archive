using MediatR;

namespace cource.archive.application.Cource.Queries.DowloadFileByName;

public class DowloadFileByNameQuery : IRequest<Stream>
{
    public string FileName { get; set; }
}