using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Queries.DowloadFileByName;

public class DowloadFileByNameHandler : IRequestHandler<DowloadFileByNameQuery, Stream>
{
    private readonly IGridFSBucket _gridFsBucket;
   
 
    public DowloadFileByNameHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
        
    }
    
    public async Task<Stream> Handle(DowloadFileByNameQuery request, CancellationToken cancellationToken)
    {
        var stream = new MemoryStream();
        await _gridFsBucket.DownloadToStreamByNameAsync(request.FileName, stream);
        stream.Position = 0;
        return stream;
    }
}