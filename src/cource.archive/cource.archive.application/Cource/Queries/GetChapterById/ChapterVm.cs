using AutoMapper;
using cource.archive.application.Mapping;
using cource.archive.domain;

namespace cource.archive.application.Cource.Queries.GetChapterById;

public class ChapterVm : IMapWith<Chapter>
{
    public string Id { get; set; }
    public string Name { get; set; }
    public string TitleId { get; set; }

    public void Mapping(Profile profile)
    {
        profile.CreateMap<Chapter, ChapterVm>()
            .ForMember(vm => vm.Id,
                opt => opt.MapFrom(ch => ch.Id.ToString()))
            .ForMember(vm => vm.Name,
                opt => opt.MapFrom(ch => ch.Name))
            .ForMember(vm => vm.TitleId,
                opt => opt.MapFrom(ch => ch.TitleId.ToString()));
    }
}