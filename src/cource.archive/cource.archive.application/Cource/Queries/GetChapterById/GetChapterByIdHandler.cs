using AutoMapper;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Queries.GetChapterById;

public class GetChapterByIdHandler : IRequestHandler<GetChapterByIdQuery, ChapterVm>
{
    private readonly IMongoCollection<Chapter> _collection;
    private readonly IMapper _mapper;

    public GetChapterByIdHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<Chapter>(settings.ChapterCollectionName);
        _mapper = mapper;
    }
    
    public async Task<ChapterVm> Handle(GetChapterByIdQuery request, CancellationToken cancellationToken)
    {
        var chapter = await _collection.FindAsync(c => c.Id == ObjectId.Parse(request.Id));
        return _mapper.Map<ChapterVm>(chapter.FirstOrDefault());
    }
}