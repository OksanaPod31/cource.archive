using cource.archive.domain;
using MediatR;

namespace cource.archive.application.Cource.Queries.GetChapterById;

public class GetChapterByIdQuery: IRequest<ChapterVm>
{
    public string Id { get; set; }
}