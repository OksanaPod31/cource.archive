using MediatR;

namespace cource.archive.application.Cource.Queries.GetFilesOfCource;

public class GetFilesOfCourceQuery : IRequest<FileStream>
{
    public string Id { get; set; }
}