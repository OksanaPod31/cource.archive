using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using MediatR;

namespace cource.archive.application.Cource.Queries.GetCourcesByName;

public class GetCourcesByNameQuery : IRequest<IList<CourceVm>>
{
    public string Name { get; set; }
}