using AutoMapper;
using cource.archive.application.Constants;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Queries.GetCourcesByTopic;

public class GetCourcesByTopicHandler : IRequestHandler<GetCourcesByTopicQuery, IList<CourceVm>>
{
    private readonly IMongoCollection<domain.CourceSpecification> _collectionSpec;
    private readonly IMongoCollection<domain.Cource> _collectionCource;
    private readonly IMapper _mapper;

    public GetCourcesByTopicHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _collectionSpec = client.GetDatabase(settings.DataBaseName)
            .GetCollection<CourceSpecification>(settings.CourceSpecification);

        _collectionCource = client.GetDatabase(settings.DataBaseName)
            .GetCollection<domain.Cource>(settings.CourceCollectionName);
        _mapper = mapper;
    }
    
    public async Task<IList<CourceVm>> Handle(GetCourcesByTopicQuery request, CancellationToken cancellationToken)
    {
        var specificationsCource = await _collectionSpec.Find(sc =>
            sc.SpecificationId == SpecificationConstants.Id.TopicTag &&
            sc.LookupValue == ObjectId.Parse(request.TopicId)).ToListAsync(cancellationToken);

        var mapList = new List<CourceVm>();
        foreach (var courceSpecification in specificationsCource)
        {
            var cource = await _collectionCource.Find(c => c.Id == courceSpecification.CourceId).FirstOrDefaultAsync(cancellationToken);
            if (cource is not null)
            {
                var durationOfCource = await _collectionSpec.Find(sc =>
                    sc.SpecificationId == SpecificationConstants.Id.DurationOfCource
                    && sc.CourceId == cource.Id).FirstOrDefaultAsync(cancellationToken);
                var item = _mapper.Map<CourceVm>(cource);
                
                item.DurationOfCource = durationOfCource?.IntValue;
                mapList.Add(item);
            }
        }

        return mapList;
    }
}