using AutoMapper;
using cource.archive.application.Mapping;

namespace cource.archive.application.Cource.Queries.GetCourcesByTopic;

public class CourceVm : IMapWith<domain.Cource>
{
    public string? Id { get; set; }
    public string? Name { get; set; }
    public string? Description { get; set; }
    public int? DurationOfCource { get; set; }
    public string? TitleId { get; set; }
    public string? RoomId { get; set; }
    
    public void Mapping(Profile profile)
    {
        profile.CreateMap<domain.Cource, CourceVm>()
            .ForMember(vm => vm.Id,
                opt => opt.MapFrom(cor => cor.Id.ToString()))
            .ForMember(vm => vm.Name,
                opt => opt.MapFrom(cor => cor.Name))
            .ForMember(vm => vm.TitleId,
                opt => opt.MapFrom(cor => cor.TitleId.ToString()))
            .ForMember(vm => vm.RoomId,
                opt => opt.MapFrom(cor => cor.RoomId));
    }
}