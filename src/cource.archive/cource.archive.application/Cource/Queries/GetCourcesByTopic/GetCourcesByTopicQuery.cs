using MediatR;

namespace cource.archive.application.Cource.Queries.GetCourcesByTopic;

public class GetCourcesByTopicQuery : IRequest<IList<CourceVm>>
{
    public string TopicId { get; set; }
}