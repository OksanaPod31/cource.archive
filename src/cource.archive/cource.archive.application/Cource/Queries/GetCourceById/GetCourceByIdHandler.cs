using AutoMapper;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Queries.GetCourceById;

public class GetCourceByIdHandler : IRequestHandler<GetCourceByIdQuery, CourceVm>
{
    private readonly IMongoCollection<domain.Cource> _collectionCource;
    private readonly IMapper _mapper;

    public GetCourceByIdHandler(IMongoDBSettings settings, IMongoClient client, IMapper mapper)
    {
        _collectionCource = client.GetDatabase(settings.DataBaseName)
            .GetCollection<domain.Cource>(settings.CourceCollectionName);
        _mapper = mapper;
    }
    
    public async Task<CourceVm> Handle(GetCourceByIdQuery request, CancellationToken cancellationToken)
    {
        var cource = await _collectionCource.Find(c => c.Id == ObjectId.Parse(request.Id)).FirstOrDefaultAsync(cancellationToken);
        return _mapper.Map<CourceVm>(cource ?? new domain.Cource());
    }
}