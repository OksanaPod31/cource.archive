using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using MediatR;

namespace cource.archive.application.Cource.Queries.GetCourceById;

public class GetCourceByIdQuery : IRequest<CourceVm>
{
    public string Id { get; set; }
}