using MediatR;
using MongoDB.Bson;

namespace cource.archive.application.Cource.Queries.DowloadFileById;

public class DowloadFileByIdQuery : IRequest<FileVm>
{
    public ObjectId Id { get; set; }
}