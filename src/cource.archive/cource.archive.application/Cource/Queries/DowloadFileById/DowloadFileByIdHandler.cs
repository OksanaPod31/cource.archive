using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Queries.DowloadFileById;

public class DowloadFileByIdHandler : IRequestHandler<DowloadFileByIdQuery, FileVm>
{
    private readonly IGridFSBucket _gridFsBucket;
   
 
    public DowloadFileByIdHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
        
    }
    
    public async Task<FileVm> Handle(DowloadFileByIdQuery request, CancellationToken cancellationToken)
    {
        var stream = new MemoryStream();
        await _gridFsBucket.DownloadToStreamAsync(request.Id, stream);
        var filter = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(request.Id));
        var fileInfo = await _gridFsBucket.FindAsync(filter);
        var type = fileInfo.FirstOrDefault().Metadata.Values.FirstOrDefault();
        
        stream.Position = 0;
        return new FileVm { FileStream = stream, Type = type.ToString()};
    }
}