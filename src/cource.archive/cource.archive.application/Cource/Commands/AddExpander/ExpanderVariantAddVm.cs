namespace cource.archive.application.Cource.Commands.AddExpander;

public class ExpanderVariantAddVm
{
    public string? ExpanderId { get; set; }
    public string ExpanderName { get; set; }
    public ExpanderVariantValueAddVm[]? ExpanderVariantValues { get; set; }
    public bool IsChanged { get; set; }
}