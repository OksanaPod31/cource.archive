namespace cource.archive.application.Cource.Commands.AddExpander;

public class ExpanderVariantValueAddVm
{
    public string? ExpanderValueId { get; set; }
    public string ExpanderValueName { get; set; }
    public bool IsChanged { get; set; }
}