using MediatR;

namespace cource.archive.application.Cource.Commands.AddExpander;

public class AddExpanderQuery : IRequest
{
    public string CourceId { get; set; }
    
    public string? ExpanderId { get; set; }
    public string ExpanderName { get; set; }
    public IList<ExpanderVariantAddVm>? ExpanderVariants { get; set; }
    public bool IsChanged { get; set; }
}