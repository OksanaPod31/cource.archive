using cource.archive.application.Constants;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Commands.AddExpander;

public class AddExpanderHandler : IRequestHandler<AddExpanderQuery>
{
    private readonly IMongoCollection<CourceSpecification> _specCollection;
    private readonly IMongoCollection<ExpanderVariant> _expanderCollection;
    private readonly IMongoCollection<ExpanderVariantValue> _expanderValueCollection;

    public AddExpanderHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _specCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<CourceSpecification>(settings.CourceSpecification);
        _expanderCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<ExpanderVariant>(settings.ExpanderVariant);
        _expanderValueCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<ExpanderVariantValue>(settings.ExpanderVariantValue);
    }
    
    public async Task<Unit> Handle(AddExpanderQuery request, CancellationToken cancellationToken)
    {
        foreach (var exp in request.ExpanderVariants)
        {
            if (exp.ExpanderId is "" or null)
            {
                var expanderVariant = new ExpanderVariant
                {
                    Id = ObjectId.GenerateNewId(),
                    StringValue = exp.ExpanderName
                };
                await _expanderCollection.InsertOneAsync(expanderVariant, cancellationToken);

                if (exp.ExpanderVariantValues?.Length > 0)
                {
                    var expanderVariantValues = new List<ExpanderVariantValue>();
                    foreach (var variantValue in exp.ExpanderVariantValues)
                    {
                        var expanderVariantValue = new ExpanderVariantValue
                        {
                            Id = ObjectId.GenerateNewId(),
                            ExpanderVariant = expanderVariant.Id,
                            Value = variantValue.ExpanderValueName
                        };
                        expanderVariantValues.Add(expanderVariantValue);
                    }

                    await _expanderValueCollection.InsertManyAsync(expanderVariantValues);
                }
                
                var spec = new CourceSpecification
                {
                    Id = ObjectId.GenerateNewId(),
                    CourceId = ObjectId.Parse(request.CourceId),
                    SpecificationId = SpecificationConstants.Id.Expander,
                    LookupValue = expanderVariant.Id,
                    StringValue = request.ExpanderName
                };

                await _specCollection.InsertOneAsync(spec, cancellationToken);
            } 
            else if (exp.IsChanged)
            {
                var update = Builders<ExpanderVariant>.Update
                    .Set(c => c.StringValue, exp.ExpanderName);
                await _expanderCollection.UpdateOneAsync(c => c.Id == ObjectId.Parse(exp.ExpanderId), update);

                foreach (var expanderVariantValue in exp.ExpanderVariantValues)
                {
                    if (expanderVariantValue.IsChanged && expanderVariantValue.ExpanderValueId is not null)
                    {
                        var updateValue = Builders<ExpanderVariantValue>.Update
                            .Set(c => c.Value, expanderVariantValue.ExpanderValueName);
                        await _expanderValueCollection.UpdateOneAsync(
                            c => c.Id == ObjectId.Parse(expanderVariantValue.ExpanderValueId), updateValue);
                    } 
                    else if (expanderVariantValue.ExpanderValueId is null)
                    {
                        await _expanderValueCollection.InsertOneAsync(new ExpanderVariantValue
                        {
                            Id = ObjectId.GenerateNewId(),
                            ExpanderVariant = ObjectId.Parse(exp.ExpanderId),
                            Value = expanderVariantValue.ExpanderValueName
                        });
                    }
                }
            }
            else if(exp.ExpanderVariantValues?.Length > 0)
            {
                foreach (var expanderVariantValue in exp.ExpanderVariantValues)
                {
                    if (expanderVariantValue.IsChanged && expanderVariantValue.ExpanderValueId is not null && expanderVariantValue.ExpanderValueId != "")
                    {
                        var updateValue = Builders<ExpanderVariantValue>.Update
                            .Set(c => c.Value, expanderVariantValue.ExpanderValueName);
                        await _expanderValueCollection.UpdateOneAsync(
                            c => c.Id == ObjectId.Parse(expanderVariantValue.ExpanderValueId), updateValue);
                    } 
                    else if (expanderVariantValue.ExpanderValueId is null || expanderVariantValue.ExpanderValueId == "")
                    {
                        await _expanderValueCollection.InsertOneAsync(new ExpanderVariantValue
                        {
                            Id = ObjectId.GenerateNewId(),
                            ExpanderVariant = ObjectId.Parse(exp.ExpanderId),
                            Value = expanderVariantValue.ExpanderValueName
                        });
                    }
                }
            }
        }
        
        if ((request.ExpanderId != "" && request.ExpanderId is not null) && request.IsChanged)
        {
            var update = Builders<CourceSpecification>.Update
                .Set(c => c.StringValue, request.ExpanderName);
            await _specCollection.UpdateManyAsync(c => c.Id == ObjectId.Parse(request.ExpanderId), update);
        }
        
        return Unit.Value;
    }
}