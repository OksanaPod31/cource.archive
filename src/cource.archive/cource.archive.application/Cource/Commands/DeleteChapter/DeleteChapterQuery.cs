using MediatR;

namespace cource.archive.application.Cource.Commands.DeleteChapter;

public class DeleteChapterQuery : IRequest
{
    public string ChapterId { get; set; }
}