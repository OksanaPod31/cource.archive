using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.DeleteChapter;

public class DeleteChapterHandler : IRequestHandler<DeleteChapterQuery>
{
    private readonly IMongoCollection<Subject> _subCollection;
    private readonly IMongoCollection<Chapter> _chapterCollection;
    private readonly IGridFSBucket _gridFsBucket;

    public DeleteChapterHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _subCollection = client.GetDatabase(settings.DataBaseName).GetCollection<Subject>(settings.Subject);
        _chapterCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<Chapter>(settings.ChapterCollectionName);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }

    public async Task<Unit> Handle(DeleteChapterQuery request, CancellationToken cancellationToken)
    {
         var subList = await _subCollection.Find(c => c.ChapterId == ObjectId.Parse(request.ChapterId)).ToListAsync(cancellationToken);
         foreach (var sub in subList)
         {
             if (sub.File != "")
             {
                 var filter = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(ObjectId.Parse(sub.File)));
                 var fileInfos = await _gridFsBucket.FindAsync(filter);
                 var fileInfo = fileInfos.FirstOrDefault();

                 if (fileInfo is not null)
                 {
                     await _gridFsBucket.DeleteAsync(fileInfo.Id);   
                 }
             }
         }

         await _subCollection.DeleteManyAsync(c => c.ChapterId == ObjectId.Parse(request.ChapterId));

         var chapter = await _chapterCollection.Find(c => c.Id == ObjectId.Parse(request.ChapterId)).FirstOrDefaultAsync(cancellationToken);
         if (chapter is not null)
         {
             var filter = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(chapter.TitleId));
             var fileInfos = await _gridFsBucket.FindAsync(filter);
             var fileInfo = fileInfos.FirstOrDefault();

             if (fileInfo is not null)
             {
                 await _gridFsBucket.DeleteAsync(fileInfo.Id);   
             }

             var result = await _chapterCollection.DeleteManyAsync(c => c.Id == chapter.Id);
             if (result.DeletedCount <= 0)
             {
                 throw new Exception("Не удалось удалить указанную главу");
             }
         }
         return Unit.Value;
    }
}