using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.CreateCource;

public class CreateCourceHandler : IRequestHandler<CreateCourceQuery, ObjectId>
{
    private readonly IMongoCollection<domain.Cource> _collection;
    private readonly IGridFSBucket _gridFsBucket;

    public CreateCourceHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<domain.Cource>(settings.CourceCollectionName);
    }
    
    public async Task<ObjectId> Handle(CreateCourceQuery request, CancellationToken cancellationToken)
    {
        var options = new GridFSUploadOptions { Metadata = new BsonDocument("type", request.FileType) };
        ObjectId id = await _gridFsBucket.UploadFromStreamAsync(request.Name, request.File, options);
        var cource = new domain.Cource
        {
            Id = MongoDB.Bson.ObjectId.GenerateNewId(),
            Name = request.Name,
            TitleId = id,
            CreatedOn = DateTime.Now,
            CreatedBy = request.UserId,
            Description = request.Description
        };
        await _collection.InsertOneAsync(cource);
        
        return cource.Id;
    }
}