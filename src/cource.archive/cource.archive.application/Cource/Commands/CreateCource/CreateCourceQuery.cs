using MediatR;
using MongoDB.Bson;

namespace cource.archive.application.Cource.Commands.CreateCource;

public class CreateCourceQuery : IRequest<ObjectId>
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string UserId { get; set; }
    public Stream File { get; set; }
    public string FileType { get; set; }
}