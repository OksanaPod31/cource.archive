using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Commands.DeleteCourcefromTopic;

public class DeleteCourcefromTopicHandler : IRequestHandler<DeleteCourcefromTopicQuery>
{
    private readonly IMongoCollection<CourceSpecification> _collectionSpec;

    public DeleteCourcefromTopicHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _collectionSpec = client.GetDatabase(settings.DataBaseName)
            .GetCollection<CourceSpecification>(settings.CourceSpecification);
    }
    
    public async Task<Unit> Handle(DeleteCourcefromTopicQuery request, CancellationToken cancellationToken)
    {
        foreach (var courceId in request.CourcesId)
        {
            var result = await _collectionSpec.DeleteOneAsync(c =>
                c.CourceId == ObjectId.Parse(courceId) && c.LookupValue == ObjectId.Parse(request.TopicId), cancellationToken);
            if (result.DeletedCount <= 0)
                throw new Exception("При удалении произошёл сбой");
        }
        return Unit.Value;
    }
}