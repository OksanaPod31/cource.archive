using MediatR;

namespace cource.archive.application.Cource.Commands.DeleteCourcefromTopic;

public class DeleteCourcefromTopicQuery : IRequest
{
    public string[] CourcesId { get; set; }
    public string TopicId { get; set; }
}