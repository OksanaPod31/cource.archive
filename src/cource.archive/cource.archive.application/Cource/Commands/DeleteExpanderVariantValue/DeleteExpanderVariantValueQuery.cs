using MediatR;

namespace cource.archive.application.Cource.Commands.DeleteExpanderVariantValue;

public class DeleteExpanderVariantValueQuery : IRequest
{
    public string ExpanderVariantValueId { get; set; }
}