using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Commands.DeleteExpanderVariantValue;

public class DeleteExpanderVariantValueHandler : IRequestHandler<DeleteExpanderVariantValueQuery>
{
    private readonly IMongoCollection<ExpanderVariantValue> _expanderValueCollection;

    public DeleteExpanderVariantValueHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _expanderValueCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<ExpanderVariantValue>(settings.ExpanderVariantValue);
    }
    
    public async Task<Unit> Handle(DeleteExpanderVariantValueQuery request, CancellationToken cancellationToken)
    {
        var result = await _expanderValueCollection.DeleteOneAsync(c => c.Id == ObjectId.Parse(request.ExpanderVariantValueId));
        if (result.DeletedCount <= 0)
        {
            throw new Exception("Не удалось удалить указанный навык");
        }
        
        return Unit.Value;
    }
}