using cource.archive.application.Cource.Queries.GetTopicList;
using MediatR;

namespace cource.archive.application.Cource.Commands.UpdateTopic;

public class UpdateTopicQuery : IRequest
{
    public string Name { get; set; }
    public string TopicId { get; set; }
    public Stream? File { get; set; }
    public string? FileType { get; set; }
}