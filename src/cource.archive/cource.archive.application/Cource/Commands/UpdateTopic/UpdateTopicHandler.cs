using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.UpdateTopic;

public class UpdateTopicHandler : IRequestHandler<UpdateTopicQuery>
{
    private readonly IMongoCollection<Topic> _topicCollection;
    private readonly IGridFSBucket _gridFsBucket;
    
    public UpdateTopicHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _topicCollection = client.GetDatabase(settings.DataBaseName).GetCollection<Topic>(settings.TopicCollection);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    
    public async Task<Unit> Handle(UpdateTopicQuery request, CancellationToken cancellationToken)
    {
        var topic = await _topicCollection.Find(c => c.Id == ObjectId.Parse(request.TopicId))
            .FirstOrDefaultAsync(cancellationToken);

        if (topic is null)
        {
            throw new Exception("Не получается найти выбранную тематику");
        }
        
        var filterTopic = new BsonDocument("_id", topic.Id);
        var updateSettingsName = new BsonDocument("$set", new BsonDocument("name", request.Name));
        await _topicCollection.UpdateOneAsync(filterTopic, updateSettingsName);

        if (request.File is not null)
        {
            var options = new GridFSUploadOptions { Metadata = new BsonDocument("type", request.FileType) };
            ObjectId id = await _gridFsBucket.UploadFromStreamAsync(request.Name, request.File, options);

            var filterTopicTitle = new BsonDocument("_id", topic.Id);
            var updateSettingsTitle = new BsonDocument("$set", new BsonDocument("title_id", id));
            
            await _topicCollection.UpdateOneAsync(filterTopicTitle, updateSettingsTitle);
        
            var filter = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(topic.TitleId));
            var fileInfos = await _gridFsBucket.FindAsync(filter);
            var fileInfo = fileInfos.FirstOrDefault();

            if (fileInfo is not null)
            {
                await _gridFsBucket.DeleteAsync(fileInfo.Id);   
            }
        }

        return Unit.Value;
    }
}