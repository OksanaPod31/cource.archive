using MediatR;

namespace cource.archive.application.Cource.Commands.AddCourceToTopic;

public class AddCourceToTopicQuery : IRequest<bool>
{
    public string[] CourceIds { get; set; }
    public string TopicId { get; set; }
}