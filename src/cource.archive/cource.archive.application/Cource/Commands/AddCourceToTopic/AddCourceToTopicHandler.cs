
using cource.archive.application.Constants;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Commands.AddCourceToTopic;

public class AddCourceToTopicHandler : IRequestHandler<AddCourceToTopicQuery, bool>
{
    private readonly IMongoCollection<CourceSpecification> _collectionSpec;
    private readonly IMongoCollection<domain.Cource> _collectionCource;
    private readonly IMongoCollection<Topic> _collectionTopic;

    public AddCourceToTopicHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _collectionSpec = client.GetDatabase(settings.DataBaseName)
            .GetCollection<CourceSpecification>(settings.CourceSpecification);
        _collectionCource = client.GetDatabase(settings.DataBaseName)
            .GetCollection<domain.Cource>(settings.CourceCollectionName);
        _collectionTopic = client.GetDatabase(settings.DataBaseName).GetCollection<Topic>(settings.TopicCollection);
    }
    
    public async Task<bool> Handle(AddCourceToTopicQuery request, CancellationToken cancellationToken)
    {
        var topic = await _collectionTopic.CountDocumentsAsync(c => c.Id == ObjectId.Parse(request.TopicId));
        if (topic <= 0 )
        {
            throw new Exception("Тематики не существует");
        }

        foreach (var id in request.CourceIds)
        {
            var cource = await _collectionCource.CountDocumentsAsync(c => c.Id == ObjectId.Parse(id));
            if (cource > 0)
            {
                var spec = new CourceSpecification
                {
                    Id = new ObjectId(),
                    CourceId = ObjectId.Parse(id),
                    LookupValue = ObjectId.Parse(request.TopicId),
                    SpecificationId = SpecificationConstants.Id.TopicTag
                };
                await _collectionSpec.InsertOneAsync(spec);
            }
        }

        return true;
    }
}