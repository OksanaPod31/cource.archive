using MediatR;

namespace cource.archive.application.Cource.Commands.DeleteExpanderVariant;

public class DeleteExpanderVariantQuery : IRequest
{
    public string ExpanderVariantId { get; set; }
}