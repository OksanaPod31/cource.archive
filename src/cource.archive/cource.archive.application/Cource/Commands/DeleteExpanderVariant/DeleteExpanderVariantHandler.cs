using cource.archive.application.Constants;
using cource.archive.application.Cource.Commands.DeleteExpanderVariantValue;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Commands.DeleteExpanderVariant;

public class DeleteExpanderVariantHandler : IRequestHandler<DeleteExpanderVariantQuery>
{
    private readonly IMongoCollection<CourceSpecification> _specCollection;
    private readonly IMongoCollection<ExpanderVariant> _expanderCollection;
    private readonly IMongoCollection<ExpanderVariantValue> _expanderValueCollection;

    public DeleteExpanderVariantHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _specCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<CourceSpecification>(settings.CourceSpecification);
        _expanderCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<ExpanderVariant>(settings.ExpanderVariant);
        _expanderValueCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<ExpanderVariantValue>(settings.ExpanderVariantValue);
    }
    
    public async Task<Unit> Handle(DeleteExpanderVariantQuery request, CancellationToken cancellationToken)
    {
        var spec = await _specCollection.Find(c =>
            c.LookupValue == ObjectId.Parse(request.ExpanderVariantId) &&
            c.SpecificationId == SpecificationConstants.Id.Expander).FirstOrDefaultAsync(cancellationToken);

        if (spec is not null)
        {
            await _specCollection.DeleteOneAsync(c => c.Id == spec.Id);
        }

        await _expanderValueCollection.DeleteManyAsync(c =>
            c.ExpanderVariant == ObjectId.Parse(request.ExpanderVariantId));
        var exp = await _expanderCollection.DeleteOneAsync(c => c.Id == ObjectId.Parse(request.ExpanderVariantId));
        if (exp.DeletedCount <= 0)
        {
            throw new Exception("Не удалось удалить блок навыков");
        }
        return Unit.Value;
    }
}