using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Commands.SetRoomIdToCource;

public class SetRoomIdToCourceHandler : IRequestHandler<SetRoomIdToCourceQuery, bool>
{
    private readonly IMongoCollection<domain.Cource> _collection;

    public SetRoomIdToCourceHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<domain.Cource>(settings.CourceCollectionName);
    }
    
    public async Task<bool> Handle(SetRoomIdToCourceQuery request, CancellationToken cancellationToken)
    {
        var filter = new BsonDocument("_id", ObjectId.Parse(request.CourceId));
        var updateSettings = new BsonDocument("$set", new BsonDocument("room_id", request.RoomId));
        var result = await _collection.UpdateOneAsync(filter, updateSettings);
        return result.ModifiedCount > 0;
    }
}