using MediatR;

namespace cource.archive.application.Cource.Commands.SetRoomIdToCource;

public class SetRoomIdToCourceQuery : IRequest<bool>
{
    public string CourceId { get; set; }
    public string RoomId { get; set; }
}