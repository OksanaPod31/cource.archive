using MediatR;
using MongoDB.Bson;

namespace cource.archive.application.Cource.Commands.AddFileToChapter;

public class AddFileToChapterQuery : IRequest<string>
{
    public string FileContent { get; set; }
    public string SubjectId { get; set; }
}