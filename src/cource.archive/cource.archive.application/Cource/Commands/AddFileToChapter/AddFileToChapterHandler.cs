using System.Text;
using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.AddFileToChapter;

public class AddFileToChapterHandler : IRequestHandler<AddFileToChapterQuery, string>
{
    private readonly IMongoCollection<Subject> _collection;
    private readonly IGridFSBucket _gridFsBucket;

    public AddFileToChapterHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<Subject>(settings.Subject);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    public async Task<string> Handle(AddFileToChapterQuery request, CancellationToken cancellationToken)
    {
        var subject = await _collection.Find(c => c.Id == ObjectId.Parse(request.SubjectId)).FirstOrDefaultAsync(cancellationToken);
        if (subject is  null)
        {
            throw new Exception("Не удаётся найти указанную подглаву");
        }

        if (!string.IsNullOrEmpty(subject.File))
        {
            var filterDel = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(ObjectId.Parse(subject.File)));
            var fileInfos = await _gridFsBucket.FindAsync(filterDel);
            var fileInfo = fileInfos.FirstOrDefault();
        
            if (fileInfo is not null)
            {
                await _gridFsBucket.DeleteAsync(fileInfo.Id);
            }
        }
        var options = new GridFSUploadOptions { Metadata = new BsonDocument("type", "text/html") };
        byte[] byteArray = Encoding.UTF8.GetBytes(request.FileContent);
        Stream stream = new MemoryStream(byteArray);
        ObjectId id = await _gridFsBucket.UploadFromStreamAsync(subject.Name, stream, options);
        
        var filter = new BsonDocument("_id", subject.Id);
        var updateSettings = new BsonDocument("$set", new BsonDocument("file", id.ToString()));
        
        await _collection.UpdateOneAsync(filter, updateSettings);
        return id.ToString();
        /*ObjectId id = await _gridFsBucket.UploadFromStreamAsync(request.FileName, request.File);
        var chapter =  _collection.FindAsync(ch => ch.Id == request.ChapterId).Result.FirstOrDefault();
        if (chapter.FilePath == null)
        {
            chapter.FilePath = new List<ObjectId>();
        }
        chapter.FilePath.Add(id);
        await _collection.ReplaceOneAsync(ch => ch.Id == chapter.Id, chapter);
        return Unit.Value;*/
    }
}