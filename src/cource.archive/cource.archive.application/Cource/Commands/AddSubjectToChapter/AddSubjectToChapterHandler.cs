using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;

namespace cource.archive.application.Cource.Commands.AddSubjectToChapter;

public class AddSubjectToChapterHandler : IRequestHandler<AddSubjectToChapterQuery, string>
{
    private readonly IMongoCollection<Subject> _subCollection;
    private readonly IMongoCollection<Chapter> _chapterCollection;

    public AddSubjectToChapterHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _subCollection = client.GetDatabase(settings.DataBaseName).GetCollection<Subject>(settings.Subject);
        _chapterCollection = client.GetDatabase(settings.DataBaseName)
            .GetCollection<Chapter>(settings.ChapterCollectionName);
    }
    
    public async Task<string> Handle(AddSubjectToChapterQuery request, CancellationToken cancellationToken)
    {
        var chapter = await _chapterCollection.FindAsync(c => c.Id == ObjectId.Parse(request.ChapterId));
        if (!chapter.Any())
        {
            throw new Exception("Не удаётся найти указанную главу");
        }

        var subject = new Subject
        {
            Id = ObjectId.GenerateNewId(),
            ChapterId = ObjectId.Parse(request.ChapterId),
            File = "",
            Name = request.SubjectName
        };
        await _subCollection.InsertOneAsync(subject);
        return subject.Id.ToString();
    }
}