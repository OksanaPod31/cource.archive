using MediatR;

namespace cource.archive.application.Cource.Commands.AddSubjectToChapter;

public class AddSubjectToChapterQuery : IRequest<string>
{
    public string ChapterId { get; set; }
    public string SubjectName { get; set; }
}