using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.UpdateCource;

public class UpdateCourceHandler : IRequestHandler<UpdateCourceQuery>
{
    private readonly IMongoCollection<domain.Cource> _courceCollection;
    private readonly IGridFSBucket _gridFsBucket;
    
    public UpdateCourceHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _courceCollection = client.GetDatabase(settings.DataBaseName).GetCollection<domain.Cource>(settings.CourceCollectionName);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    
    public async Task<Unit> Handle(UpdateCourceQuery request, CancellationToken cancellationToken)
    {
        var cource = await _courceCollection.Find(c => c.Id == ObjectId.Parse(request.CourceId)).FirstOrDefaultAsync(cancellationToken);

        if (cource is null)
        {
            throw new Exception("Не удается найти указанный курс");
        }
        
        var filterTopic = new BsonDocument("_id", cource.Id);
        var updateSettingsName = new BsonDocument("$set", new BsonDocument("name", request.Name));
        var updateSettingsDescription = new BsonDocument("$set", new BsonDocument("description", request.Description));
        await _courceCollection.UpdateOneAsync(filterTopic, updateSettingsName);
        await _courceCollection.UpdateOneAsync(filterTopic, updateSettingsDescription);
        
        if (request.File is not null)
        {
            var options = new GridFSUploadOptions { Metadata = new BsonDocument("type", request.FileType) };
            ObjectId id = await _gridFsBucket.UploadFromStreamAsync(request.Name, request.File, options);

            var filterTopicTitle = new BsonDocument("_id", cource.Id);
            var updateSettingsTitle = new BsonDocument("$set", new BsonDocument("title_id", id));
            
            await _courceCollection.UpdateOneAsync(filterTopicTitle, updateSettingsTitle);
        
            var filter = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(cource.TitleId));
            var fileInfos = await _gridFsBucket.FindAsync(filter);
            var fileInfo = fileInfos.FirstOrDefault();

            if (fileInfo is not null)
            {
                await _gridFsBucket.DeleteAsync(fileInfo.Id);   
            }
        }

        return Unit.Value;
    }
}