using MediatR;

namespace cource.archive.application.Cource.Commands.UpdateCource;

public class UpdateCourceQuery : IRequest
{
    public string Name { get; set; }
    public string Description { get; set; }
    public string CourceId { get; set; }
    public Stream? File { get; set; }
    public string? FileType { get; set; }
}