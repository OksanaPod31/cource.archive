using MediatR;

namespace cource.archive.application.Cource.Commands.ModifyChapter;

public class ModifyChapterQuery : IRequest
{
    public string? Id { get; set; }
    public string Name { get; set; }
    public string CourceId { get; set; }
    public Stream? File { get; set; }
    public string? FileType { get; set; }
}