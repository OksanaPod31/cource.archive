using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.ModifyChapter;

public class ModifyChapterHandler : IRequestHandler<ModifyChapterQuery>
{
    private readonly IMongoCollection<Chapter> _chapterCollection;
    private readonly IGridFSBucket _gridFsBucket;

    public ModifyChapterHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _chapterCollection = client.GetDatabase(settings.DataBaseName).GetCollection<Chapter>(settings.ChapterCollectionName);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    
    public async Task<Unit> Handle(ModifyChapterQuery request, CancellationToken cancellationToken)
    {
        if (request.Id is not null)
        {
            var chapter = await _chapterCollection.Find(c => c.Id == ObjectId.Parse(request.Id))
                .FirstOrDefaultAsync(cancellationToken);

            if (chapter is not null)
            {
                if (request.File is not null)
                {
                    var options = new GridFSUploadOptions { Metadata = new BsonDocument("type", request.FileType) };
                    ObjectId id = await _gridFsBucket.UploadFromStreamAsync(request.Name, request.File, options);

                    await _chapterCollection.UpdateOneAsync(c => c.Id == chapter.Id, new BsonDocument("$set", new BsonDocument("title_id", id)));

                    var filter = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(chapter.TitleId));
                    var fileInfos = await _gridFsBucket.FindAsync(filter);
                    var fileInfo = fileInfos.FirstOrDefault();

                    if (fileInfo is not null)
                    {
                        await _gridFsBucket.DeleteAsync(fileInfo.Id);   
                    }
                }
                if (request.Name != chapter.Name)
                {
                    await _chapterCollection.UpdateOneAsync(c => c.Id == chapter.Id, new BsonDocument("$set", new BsonDocument("name", request.Name)));
                }
            }
        }
        else
        {
            var options = new GridFSUploadOptions { Metadata = new BsonDocument("type", request.FileType) };
            ObjectId id = await _gridFsBucket.UploadFromStreamAsync(request.Name, request.File, options);

            var chapter = new Chapter
            {
                Id = ObjectId.GenerateNewId(),
                CourceId = request.CourceId,
                CreatedOn = DateTime.Now,
                Name = request.Name,
                TitleId = id
            };

            await _chapterCollection.InsertOneAsync(chapter, cancellationToken);
        }
        return Unit.Value;
    }
}