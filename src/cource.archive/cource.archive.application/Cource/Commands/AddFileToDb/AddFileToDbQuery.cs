using MediatR;

namespace cource.archive.application.Cource.Commands.AddFileToDb;

public class AddFileToDbQuery : IRequest<string>
{
    public string Name { get; set; }
    public Stream File { get; set; }
    public string FileType { get; set; }
}