using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.AddFileToDb;

public class AddFileToDbHandler : IRequestHandler<AddFileToDbQuery, string>
{
    private readonly IGridFSBucket _gridFsBucket;

    public AddFileToDbHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    
    public async Task<string> Handle(AddFileToDbQuery request, CancellationToken cancellationToken)
    {
        var options = new GridFSUploadOptions { Metadata = new BsonDocument("type", request.FileType) };
        ObjectId id = await _gridFsBucket.UploadFromStreamAsync(request.Name, request.File, options);
        return id.ToString();
    }
}