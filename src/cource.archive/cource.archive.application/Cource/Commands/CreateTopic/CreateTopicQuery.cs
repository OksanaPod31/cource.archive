using MediatR;
using MongoDB.Bson;

namespace cource.archive.application.Cource.Commands.CreateTopic;

public class CreateTopicQuery : IRequest<ObjectId>
{
    public string Name { get; set; }
    public string FileType { get; set; }
    public string UserId { get; set; }
    
    public Stream File { get; set; }
}