using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.CreateTopic;

public class CreateTopicHandler : IRequestHandler<CreateTopicQuery, ObjectId>
{
    private readonly IMongoCollection<domain.Topic> _collection;
    private readonly IGridFSBucket _gridFsBucket;

    public CreateTopicHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<domain.Topic>(settings.TopicCollection);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    
    public async Task<ObjectId> Handle(CreateTopicQuery request, CancellationToken cancellationToken)
    {
        var options = new GridFSUploadOptions { Metadata = new BsonDocument("type", request.FileType) };
        ObjectId id = await _gridFsBucket.UploadFromStreamAsync(request.Name, request.File, options);
        var topic = new Topic
        {
            Id = MongoDB.Bson.ObjectId.GenerateNewId(),
            Name = request.Name,
            CreatedBy = request.UserId,
            CreatedOn = DateTime.Now,
            TitleId = id
        };

        await _collection.InsertOneAsync(topic);
        return topic.Id;
    }
}