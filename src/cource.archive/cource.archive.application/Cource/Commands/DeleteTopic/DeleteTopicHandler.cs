using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.DeleteTopic;

public class DeleteTopicHandler : IRequestHandler<DeleteTopicQuery>
{
    private readonly IMongoCollection<CourceSpecification> _courceSpecification;
    private readonly IMongoCollection<Topic> _topicCollection;
    private readonly IGridFSBucket _gridFsBucket; 

    public DeleteTopicHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _courceSpecification = client.GetDatabase(settings.DataBaseName)
            .GetCollection<CourceSpecification>(settings.CourceSpecification);
        _topicCollection = client.GetDatabase(settings.DataBaseName).GetCollection<Topic>(settings.TopicCollection);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    public async Task<Unit> Handle(DeleteTopicQuery request, CancellationToken cancellationToken)
    {
        var topic = await _topicCollection.Find(c => c.Id == ObjectId.Parse(request.TopicId))
            .FirstOrDefaultAsync(cancellationToken);
        var topicDeleted = await _topicCollection.DeleteOneAsync(t => t.Id == ObjectId.Parse(request.TopicId));
        if (topicDeleted.DeletedCount <= 0)
        {
            throw new Exception("Не удалось удалить указанную тематику");
        }
        await _courceSpecification.DeleteManyAsync(t => t.LookupValue == ObjectId.Parse(request.TopicId));
        
        var filter = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(topic.TitleId));
        var fileInfos = await _gridFsBucket.FindAsync(filter);
        var fileInfo = fileInfos.FirstOrDefault();
        if (fileInfo is not null)
        {
            await _gridFsBucket.DeleteAsync(fileInfo.Id);
        }

        return Unit.Value;
    }
}