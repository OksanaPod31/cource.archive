using MediatR;

namespace cource.archive.application.Cource.Commands.DeleteTopic;

public class DeleteTopicQuery : IRequest
{
    public string TopicId { get; set; }
}