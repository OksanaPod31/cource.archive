using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.DeleteCource;

public class DeleteCourceHandler : IRequestHandler<DeleteCourceQuery>
{
    private readonly IMongoCollection<domain.Cource> _collection;
    private readonly IMongoCollection<CourceSpecification> _courceSpecification;
    private readonly IGridFSBucket _gridFsBucket; 
    
    public DeleteCourceHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _courceSpecification = client.GetDatabase(settings.DataBaseName)
            .GetCollection<CourceSpecification>(settings.CourceSpecification);
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<domain.Cource>(settings.CourceCollectionName);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    
    public async Task<Unit> Handle(DeleteCourceQuery request, CancellationToken cancellationToken)
    {
        var cource = await _collection.Find(c => c.Id == ObjectId.Parse(request.CourceId))
            .FirstOrDefaultAsync(cancellationToken);
        var deleteCourceResult = await _collection.DeleteOneAsync(c => c.Id == ObjectId.Parse(request.CourceId));

        if (deleteCourceResult.DeletedCount <= 0)
        {
            throw new Exception("Не удалось удалить курс");
        }
        await _courceSpecification.DeleteManyAsync(c => c.CourceId == ObjectId.Parse(request.CourceId));
        
        var filter = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(cource.TitleId));
        var fileInfos = await _gridFsBucket.FindAsync(filter);
        var fileInfo = fileInfos.FirstOrDefault();
        
        if (fileInfo is not null)
        {
            await _gridFsBucket.DeleteAsync(fileInfo.Id);
        }
        return Unit.Value;
    }
}