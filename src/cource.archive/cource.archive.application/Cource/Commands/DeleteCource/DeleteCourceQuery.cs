using MediatR;

namespace cource.archive.application.Cource.Commands.DeleteCource;

public class DeleteCourceQuery : IRequest
{
    public string CourceId { get; set; }
}