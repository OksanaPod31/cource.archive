using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.CreateChapter;

public class CreateChapterHandler : IRequestHandler<CreateChapterQuery, string>
{
    private readonly IMongoCollection<domain.Chapter> _collection;
    private readonly IGridFSBucket _gridFsBucket;

    public CreateChapterHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _collection = client.GetDatabase(settings.DataBaseName).GetCollection<domain.Chapter>(settings.ChapterCollectionName);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    
    public async Task<string> Handle(CreateChapterQuery request, CancellationToken cancellationToken)
    {
        var options = new GridFSUploadOptions { Metadata = new BsonDocument("type", request.FileType) };
        ObjectId id = await _gridFsBucket.UploadFromStreamAsync(request.Name, request.File, options);
        var chapter = new Chapter
        {
            Id = MongoDB.Bson.ObjectId.GenerateNewId(),
            Name = request.Name,
            TitleId = id,
            CourceId = request.CourceId,
            CreatedOn = DateTime.Now
        };

        await _collection.InsertOneAsync(chapter);
        return chapter.Id.ToString();
    }
}