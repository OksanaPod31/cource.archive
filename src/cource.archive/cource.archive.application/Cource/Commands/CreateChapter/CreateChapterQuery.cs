using MediatR;
using MongoDB.Bson;

namespace cource.archive.application.Cource.Commands.CreateChapter;

public class CreateChapterQuery : IRequest<string>
{
    public string Name { get; set; }
    public string CourceId { get; set; }
    public Stream File { get; set; }
    public string FileType { get; set; }
}