using cource.archive.domain;
using cource.archive.domain.Settings;
using MediatR;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.GridFS;

namespace cource.archive.application.Cource.Commands.DeleteSubject;

public class DeleteSubjectHandler : IRequestHandler<DeleteSubjectQuery>
{
    private readonly IMongoCollection<Subject> _subCollection;
    private readonly IGridFSBucket _gridFsBucket;

    public DeleteSubjectHandler(IMongoDBSettings settings, IMongoClient client)
    {
        _subCollection = client.GetDatabase(settings.DataBaseName).GetCollection<Subject>(settings.Subject);
        _gridFsBucket = new GridFSBucket(client.GetDatabase(settings.FileDataBase));
    }
    
    public async Task<Unit> Handle(DeleteSubjectQuery request, CancellationToken cancellationToken)
    {
        var subject = await _subCollection.Find(c => c.Id == ObjectId.Parse(request.SubjectId))
            .FirstOrDefaultAsync(cancellationToken);

        if (subject is null)
            throw new Exception("Не удалось найти указанную тему главы");
        
        var filter = Builders<GridFSFileInfo>.Filter.Eq(info => info.IdAsBsonValue, BsonValue.Create(subject.File));
        var fileInfos = await _gridFsBucket.FindAsync(filter);
        var fileInfo = fileInfos.FirstOrDefault();
        if (fileInfo is not null)
        {
            await _gridFsBucket.DeleteAsync(fileInfo.Id);
        }

        await _subCollection.DeleteOneAsync(c => c.Id == subject.Id);
        return Unit.Value;
    }
}