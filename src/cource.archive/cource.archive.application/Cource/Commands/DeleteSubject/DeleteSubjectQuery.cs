using MediatR;

namespace cource.archive.application.Cource.Commands.DeleteSubject;

public class DeleteSubjectQuery : IRequest
{
    public string SubjectId { get; set; }
}