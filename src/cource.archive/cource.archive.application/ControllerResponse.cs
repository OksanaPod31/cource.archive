namespace cource.archive.application;

public class ControllerResponse<T>
{
    public bool Success { get; set; }
    public string ErrorMessage { get; set; }
    public T Response { get; set; }
    public ControllerResponse(T response)
    {
        Response = response;
        Success = true;
    }

    public ControllerResponse(Exception error)
    {
        ErrorMessage = error.Message;
        Success = false;
    }
}