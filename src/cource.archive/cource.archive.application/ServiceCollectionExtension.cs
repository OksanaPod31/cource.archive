using System.Reflection;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace cource.archive.application;

public static class ServiceCollectionExtension
{
    public static void AddMediatr(this IServiceCollection services)
    {
        services.AddMediatR(Assembly.GetExecutingAssembly());
        //services.AddMediatR(cfg=>cfg.RegisterServicesFromAssemblies(Assembly.GetExecutingAssembly()));
    }
}