using MongoDB.Bson;

namespace cource.archive.application.Constants;

public static class SpecificationConstants
{
    public  static class Id
    {
        public static ObjectId TopicTag = ObjectId.Parse("661417926086a0a8a5f091e6"); 
        public static ObjectId Expander = ObjectId.Parse("66311b6468c1d408e895c9a5"); 
        public static ObjectId DurationOfCource = ObjectId.Parse("6630f36f68c1d408e895c99c"); 
    }
}