namespace cource.archive.domain.Settings;

public interface IMongoDBSettings
{
    public string DataBaseName { get; set; }
    public string FileDataBase { get; set; }
    public string CourceCollectionName { get; set; }
    public string ChapterCollectionName { get; set; }
    public string TopicCollection { get; set; }
    public string CourceSpecification { get; set; }
    public string ExpanderVariant { get; set; }
    public string ExpanderVariantValue { get; set; }
    public string Subject { get; set; }
}
