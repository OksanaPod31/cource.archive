namespace cource.archive.domain.Settings;

public class MongoDBSettings : IMongoDBSettings
{
    public string DataBaseName { get; set; } = null!;
    public string FileDataBase { get; set; } = null!;
    public string CourceCollectionName { get; set; } = null!;
    public string ChapterCollectionName { get; set; } = null!;
    public string TopicCollection { get; set; }
    public string CourceSpecification { get; set; }
    public string ExpanderVariant { get; set; }
    public string ExpanderVariantValue { get; set; }
    public string Subject { get; set; }
}