using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace cource.archive.domain;

public class Chapter : BaseMongoEntity
{
    [BsonElement("name")]
    public string? Name { get; set; }
    
    [BsonElement("file_path")] 
    public string FilePath { get; set; }
    
    [BsonElement("cource")]
    public string? CourceId { get; set; }
}