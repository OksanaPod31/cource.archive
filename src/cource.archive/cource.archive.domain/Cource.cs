using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace cource.archive.domain;

public class Cource : BaseMongoEntity
{
    [BsonElement("name")]
    public string? Name { get; set; }

    [BsonElement("description")]
    public string? Description { get; set; }
    
    [BsonElement("room_id")]
    public string? RoomId { get; set; }
    
    [BsonElement("file_path")]
    public string[]? FilePath { get; set; }
}