using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace cource.archive.domain;

public class ExpanderVariantValue
{
    [BsonId]
    public ObjectId Id { get; set; }
    
    [BsonElement("expanderVariant")]
    public ObjectId ExpanderVariant { get; set; }
    
    [BsonElement("value")]
    public string Value { get; set; }
}