using MongoDB.Bson.Serialization.Attributes;

namespace cource.archive.domain;

public class Topic : BaseMongoEntity
{

    /*public Topic()
    {
        TitleFile = new MemoryStream();
        TitleFile.Position = 0;
    }*/
    
    [BsonElement("name")]
    public string? Name { get; set; }
    
}