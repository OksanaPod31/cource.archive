using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace cource.archive.domain;

public class CourceSpecification
{
    [BsonId] public ObjectId Id { get; set; } = new ObjectId();
    
    [BsonElement("courceId")]
    public ObjectId CourceId { get; set; }
    
    [BsonElement("specificationId")]
    public ObjectId SpecificationId { get; set; }
    
    [BsonElement("stringValue")]
    public string? StringValue { get; set; }

    [BsonElement("boolValue")] 
    public bool BoolValue { get; set; }

    [BsonElement("intValue")] 
    public int IntValue { get; set; } = 0;
    
    [BsonElement("lookupValue")]
    public ObjectId? LookupValue { get; set; }
}