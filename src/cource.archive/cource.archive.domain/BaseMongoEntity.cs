using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace cource.archive.domain;

public class BaseMongoEntity
{
    [BsonId]
    public ObjectId Id { get; set; }

    [BsonElement("created_on")]
    public DateTime? CreatedOn { get; set; }
    
    [BsonElement("created_by")]
    public string? CreatedBy { get; set; }
    
    [BsonElement("title_id")] 
    public ObjectId TitleId { get; set; }
}