using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace cource.archive.domain;

public class Subject
{
    [BsonId]
    public ObjectId Id { get; set; }
    
    [BsonElement("name")]
    public string Name { get; set; }
    
    [BsonElement("chapterId")]
    public ObjectId ChapterId { get; set; }
    
    [BsonElement("file")]
    public string? File { get; set; }
}