using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace cource.archive.domain;

public class ExpanderVariant
{
    [BsonId]
    public ObjectId Id { get; set; }
    
    [BsonElement("stringValue")]
    public string StringValue { get; set; }
}