﻿using Microsoft.AspNetCore.Mvc;

namespace cource.archive.Services
{
    public interface IFileService
    {
        FileStream GetOneFile();
        FileStream GetAllFiles();
    }
}
