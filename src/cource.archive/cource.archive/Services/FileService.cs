﻿using Microsoft.AspNetCore.Mvc;
using System.Web;
using System.IO;
using System.Net.Http.Headers;
using Microsoft.Extensions.FileProviders;
using System.IO.Compression;

namespace cource.archive.Services
{
    public class FileService : IFileService
    {
        private readonly IWebHostEnvironment _appEnvironment;
        private readonly string _basePath;
        

        public FileService(IWebHostEnvironment appEnvironment)
        {
            _appEnvironment = appEnvironment;
            _basePath = Path.Combine(_appEnvironment.ContentRootPath, "Files");
        }

        public FileStream GetAllFiles()
        {
            List<string> files = new List<string>
                {
                    "Геймдизайн.pdf",
                    "root (1).xlsx"
                };

            var fs = new FileStream(Path.Combine(_basePath, "archive.zip"), FileMode.OpenOrCreate);

            using (var memoryStream = new MemoryStream())
            {
                using (var zipArchive = new ZipArchive(memoryStream, ZipArchiveMode.Create, true))
                {
                    foreach (var file in files)
                    {
                        zipArchive.CreateEntryFromFile(Path.Combine(_basePath, file), file);
                    }
                }

                memoryStream.Position = 0;
                fs.Position = 0;
                memoryStream.WriteTo(fs);
            }

            return fs;
        }

        public FileStream GetOneFile()
        {
            var path = Path.Combine(_basePath, "Геймдизайн.pdf");
            var fs = new FileStream(path, FileMode.Open);

            return fs;
        }

        /*public FileResult Sendfiles()
        {
            *//*var fileProvider = new PhysicalFileProvider(Directory.GetCurrentDirectory());
            var fileinfo = fileProvider.GetFileInfo("Геймдизайн.pdf");*//*
            ControllerContext.
            *//*var filePath = Path.Combine(_appEnvironment.ContentRootPath, "Files/Геймдизайн.pdf");
            var bytes = System.IO.File.ReadAllBytes(filePath);
            string file_type = "application/octet-stream";
            string file_name = "Геймдизайн.pdf";
            return System.Web.HttpUtility.Fil
            return new FileContentResult(bytes, file_type);*/
        /* using var multipartFormContent = new MultipartFormDataContent();
         var fileStreamContent = new StreamContent(File.OpenRead(filePath)); fileStreamContent.Headers.ContentType = new MediaTypeHeaderValue("multipart/form-data");
         multipartFormContent.Add(fileStreamContent, name: "file", fileName: "Геймдизайн.pdf");
         using var response = await httpClient.PostAsync(serverAddress, multipartFormContent); var responseText = await response.Content.ReadAsStringAsync();
         Console.WriteLine(responseText);*//*
    }*/
    }
}
