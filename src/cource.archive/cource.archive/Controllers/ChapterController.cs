using cource.archive.application;
using cource.archive.application.Cource.Commands.CreateChapter;
using cource.archive.application.Cource.Commands.DeleteChapter;
using cource.archive.application.Cource.Commands.ModifyChapter;
using cource.archive.application.Cource.Queries.GetChapterByCourceId;
using cource.archive.application.Cource.Queries.GetChapterById;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace cource.archive.Controllers;

[ApiController]
[EnableCors("CorsPolicy")]
[Route("api/[controller]")]
public class ChapterController : Controller
{
    private IMediator _mediator;
    protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    
    [HttpPost("CreateChapter")]
    public async Task<string> CreateChapter(IFormFile file, string name, string courceId)
    {
        var fileType = file.ContentType;
        var fileStream = file.OpenReadStream();
        var query = new CreateChapterQuery
        {
            Name = name,
            CourceId = courceId,
            File = fileStream,
            FileType = fileType
        };
        return await Mediator.Send(query);
    }
    
    [HttpGet("GetChaptersByCourceId")]
    public async Task<IList<ChapterVm>> GetChaptersByCourceId([FromQuery] GetChaptersByCourceIdQuery query)
    {
        var collection = await Mediator.Send(query);
        return collection;
    }
    
    [HttpGet("DeleteChapter")]
    public async Task<ControllerResponse<Unit>> DeleteChapter([FromQuery] DeleteChapterQuery query)
    {
        try
        {
            await Mediator.Send(query);
            return new ControllerResponse<Unit>(Unit.Value);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }
    
    [HttpGet("GetChapterById")]
    public async Task<ChapterVm> GetChapterById([FromQuery] GetChapterByIdQuery query)
    {
        return await Mediator.Send(query);
    }
    
    [HttpPost("ModifyChapter")]
    public async Task<ControllerResponse<Unit>> ModifyChapter([FromForm] IFormFile? file, [FromForm]string name, [FromForm]string courceId, [FromForm] string? id)
    {
        try
        {
            var query = new ModifyChapterQuery {Name = name, CourceId = courceId, Id = id};
            if (file is not null)
            {
                var fileType = file.ContentType;
                var fileStream = file.OpenReadStream();
                query.File = fileStream;
                query.FileType = fileType;
            }
            await Mediator.Send(query);
            return new ControllerResponse<Unit>(Unit.Value);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
        
    }
}