﻿using cource.archive.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using System.IO;
using cource.archive.application.Cource.Commands.AddFileToDb;
using MediatR;
using Microsoft.AspNetCore.Cors;

namespace cource.archive.Controllers
{
    [ApiController]
    [EnableCors("CorsPolicy")]
    [Route("api/[controller]")]
    public class FileController : ControllerBase
    {
        private IMediator _mediator;
        protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
        
        [HttpPost("AddFileToDb")]
        public async Task<string> AddFileToDb(IFormFile file, string name)
        {
            var fileType = file.ContentType;
            var fileStream = file.OpenReadStream();
            
            return await Mediator.Send(new AddFileToDbQuery {File = fileStream, Name = name, FileType = fileType});
        }
    }
}
