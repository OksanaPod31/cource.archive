using cource.archive.application.Cource.Queries.GetSubjectsByChapterId;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace cource.archive.Controllers;

public class SubjectController : Controller
{
    private IMediator _mediator;
    protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    
    [HttpGet("GetSubjectByChapterId")]
    public async Task<IList<SubjectVm>> GetSubjectByChapterId([FromQuery] GetSubjectsByChapterIdQuery query)
    {
        var collection = await Mediator.Send(query);
        return collection;
    }
}