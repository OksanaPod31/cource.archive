using cource.archive.application;
using cource.archive.application.Cource.Queries.GetAvailableCourceIcludingTopic;
using cource.archive.application.Cource.Queries.GetAvailableCourcesNotIncludingTopic;
using cource.archive.application.Cource.Queries.GetCourceDescription;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.application.Cource.Queries.GetWorks;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;

namespace cource.archive.Controllers;

[ApiController]
//[EnableCors("CorsPolicy")]
[Route("api/[controller]")]
public class UserController : Controller
{
    private IMediator _mediator;
    protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();
    
    [HttpPost("GetUserWorks")]
    public async Task<WorksVm> GetUserWorks([FromBody] GetWorksQuery query)
    {
        var works =  await Mediator.Send(query);
        return works;
    }

    [HttpPost("GetAvailableCourcesNotIncludingTopic")]
    public async Task<ControllerResponse<IList<CourceVm>>> GetAvailableCourcesNotIncludingTopic([FromBody] GetAvailableCourcesNotIncludingTopicQuery query)
    {
        try
        {
            var cources = await Mediator.Send(query);
            return new ControllerResponse<IList<CourceVm>>(cources);
        }
        catch (Exception e)
        {
            return new ControllerResponse<IList<CourceVm>>(e);
        }
    }
    
    [HttpPost("GetAvailableCourcesIncludingTopic")]
    public async Task<ControllerResponse<IList<CourceVm>>> GetAvailableCourcesIncludingTopic([FromBody] GetAvailableCourceIcludingTopicQuery query)
    {
        try
        {
            var cources = await Mediator.Send(query);
            return new ControllerResponse<IList<CourceVm>>(cources);
        }
        catch (Exception e)
        {
            return new ControllerResponse<IList<CourceVm>>(e);
        }
    }
}