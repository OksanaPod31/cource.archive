using cource.archive.application;
using cource.archive.application.Cource.Commands.AddCourceToTopic;
using cource.archive.application.Cource.Commands.AddExpander;
using cource.archive.application.Cource.Commands.AddFileToChapter;
using cource.archive.application.Cource.Commands.AddSubjectToChapter;
using cource.archive.application.Cource.Commands.CreateCource;
using cource.archive.application.Cource.Commands.CreateChapter;
using cource.archive.application.Cource.Commands.CreateTopic;
using cource.archive.application.Cource.Commands.DeleteCource;
using cource.archive.application.Cource.Commands.DeleteCourcefromTopic;
using cource.archive.application.Cource.Commands.DeleteExpanderVariant;
using cource.archive.application.Cource.Commands.DeleteExpanderVariantValue;
using cource.archive.application.Cource.Commands.DeleteSubject;
using cource.archive.application.Cource.Commands.DeleteTopic;
using cource.archive.application.Cource.Commands.SetRoomIdToCource;
using cource.archive.application.Cource.Commands.UpdateCource;
using cource.archive.application.Cource.Commands.UpdateTopic;
using cource.archive.application.Cource.Queries.DowloadFileById;
using cource.archive.application.Cource.Queries.DowloadFileByName;
using cource.archive.application.Cource.Queries.GetChapterByCourceId;
using cource.archive.application.Cource.Queries.GetChapterById;
using cource.archive.application.Cource.Queries.GetCourceById;
using cource.archive.application.Cource.Queries.GetCourceDescription;
using cource.archive.application.Cource.Queries.GetCourceList;
using cource.archive.application.Cource.Queries.GetCourcesByName;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.application.Cource.Queries.GetFilesFromChapter;
using cource.archive.application.Cource.Queries.GetSubjectsByChapterId;
using cource.archive.application.Cource.Queries.GetTopicList;
using cource.archive.domain;
using MediatR;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Bson;

namespace cource.archive.Controllers;

[ApiController]
[EnableCors("CorsPolicy")]
[Route("api/[controller]")]
public class CourceController : ControllerBase
{
    private IMediator _mediator;
    protected IMediator Mediator => _mediator ??= HttpContext.RequestServices.GetService<IMediator>();

    [HttpPost("CreateCource")]
    public async Task<ControllerResponse<string>> CreateCource([FromForm] IFormFile file, [FromForm] string name, [FromForm] string description, [FromForm]string userId)
    {
        try
        {
            var fileType = file.ContentType;
            var fileStream = file.OpenReadStream();
            var courceId = await Mediator.Send(new CreateCourceQuery {Name = name, Description = description, File = fileStream, FileType = fileType, UserId = userId});
            return new ControllerResponse<string>(courceId.ToString());
        }
        catch (Exception e)
        {
            return new ControllerResponse<string>(e);
        }
    }

    [HttpPost("CreateTopic")]
    public async Task<string> CreateTopic([FromForm] IFormFile file, [FromForm]string name, [FromForm]string userId)
    {
        var fileType = file.ContentType;
        var fileStream = file.OpenReadStream();
        var id = await Mediator.Send(new CreateTopicQuery{ File = fileStream, Name = name, FileType = fileType, UserId = userId});
        return id.ToString();
    }
    
    [HttpPost("UpdateTopic")]
    public async Task<ControllerResponse<Unit>> UpdateTopic([FromForm] IFormFile? file, [FromForm]string name, [FromForm]string idTopic )
    {
        try
        {
            var request = new UpdateTopicQuery
                { Name = name, TopicId = idTopic };
            if (file is not null)
            {
                var fileType = file.ContentType;
                var fileStream = file.OpenReadStream();
                request.File = fileStream;
                request.FileType = fileType;
            }
            var result = await Mediator.Send(request);
            return new ControllerResponse<Unit>(result);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }
    
    [HttpPost("UpdateCource")]
    public async Task<ControllerResponse<Unit>> UpdateCource([FromForm] IFormFile? file, [FromForm]string name, [FromForm]string courceId, [FromForm] string description )
    {
        try
        {
            var request = new UpdateCourceQuery
                { Name = name, CourceId = courceId, Description = description};
            if (file is not null)
            {
                var fileType = file.ContentType;
                var fileStream = file.OpenReadStream();
                request.File = fileStream;
                request.FileType = fileType;
            }
            var result = await Mediator.Send(request);
            return new ControllerResponse<Unit>(result);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }

    [HttpPost("AddCourceToTopic")]
    public async Task<ControllerResponse<bool>> AddCourceToTopic([FromForm] AddCourceToTopicQuery query)
    {
        try
        {
           var response = await Mediator.Send(query);
           return new ControllerResponse<bool>(response);
        }
        catch (Exception e)
        {
            return new ControllerResponse<bool>(e);
        }
    }
    [HttpPost("DeleteCourcefromTopic")]
    public async Task<ControllerResponse<Unit>> DeleteCourcefromTopic([FromForm] DeleteCourcefromTopicQuery query)
    {
        try
        {
           var response = await Mediator.Send(query);
           return new ControllerResponse<Unit>(response);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }
    
    [HttpPost("AddExpander")]
    public async Task<ControllerResponse<Unit>> AddExpander([FromBody] AddExpanderQuery query)
    {
        try
        {
           var response = await Mediator.Send(query);
           return new ControllerResponse<Unit>(response);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }

    [HttpGet("GetCourceList")]
    public async Task<IList<CourceVm>> GetCourceList([FromQuery] GetCourceListQuery query)
    {
        var collection = await Mediator.Send(query);
        return collection;
    }
    
    [HttpGet("DeleteExpanderVariantValue")]
    public async Task<ControllerResponse<Unit>> DeleteExpanderVariantValue([FromQuery] DeleteExpanderVariantValueQuery query)
    {
        try
        {
            var result = await Mediator.Send(query);
            return new ControllerResponse<Unit>(result);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }
    
    [HttpGet("DeleteExpanderVariant")]
    public async Task<ControllerResponse<Unit>> DeleteExpanderVariant([FromQuery] DeleteExpanderVariantQuery query)
    {
        try
        {
            var result = await Mediator.Send(query);
            return new ControllerResponse<Unit>(result);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }

    [HttpGet("DeleteTopic")]
    public async Task<ControllerResponse<Unit>> DeleteTopic([FromQuery] DeleteTopicQuery request)
    {
        try
        {
            await Mediator.Send(request);
            return new ControllerResponse<Unit>(Unit.Value);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }
    
    [HttpGet("DeleteCource")]
    public async Task<ControllerResponse<Unit>> DeleteCource([FromQuery] DeleteCourceQuery request)
    {
        try
        {
            await Mediator.Send(request);
            return new ControllerResponse<Unit>(Unit.Value);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }

    [HttpPost("AddFileToChapter")]
    public async Task<ControllerResponse<string>> AddFileToChapter([FromBody] AddFileToChapterQuery query)
    {
        try
        {
            var response = await Mediator.Send(query);
            return new ControllerResponse<string>(response);
        }
        catch (Exception e)
        {
            return new ControllerResponse<string>(e);
        }
    }
    
    [HttpPost("AddSubjectToChapter")]
    public async Task<ControllerResponse<string>> AddSubjectToChapter([FromBody] AddSubjectToChapterQuery query)
    {
        try
        {
            var response = await Mediator.Send(query);
            return new ControllerResponse<string>(response);
        }
        catch (Exception e)
        {
            return new ControllerResponse<string>(e);
        }
    }
    
    [HttpGet("GetFilesFromChapter")]
    public async Task<FileStreamResult> GetFilesFromChapter([FromQuery] string[] fileNames)
    {
        var list = new List<string>();
        list.AddRange(fileNames);
        var result = await Mediator.Send(new GetFilesFromChapterQuery {FileNames = list});
        return File(result, "application/zip", "my.zip");
    }

    [HttpGet("GetTopicList")]
    public async Task<IList<TopicVm>> GetTopicList([FromQuery] GetTopicListQuery query)
    {
        var t =  await Mediator.Send(query);
        
        return t;
    }
    
    [HttpGet("DeleteSubject")]
    public async Task<ControllerResponse<Unit>> DeleteSubject([FromQuery] DeleteSubjectQuery query)
    {
        try
        {
            var response = await Mediator.Send(query);
            return new ControllerResponse<Unit>(response);
        }
        catch (Exception e)
        {
            return new ControllerResponse<Unit>(e);
        }
    }
    
    [HttpPost("GetCourcesByName")]
    public async Task<ControllerResponse<IList<CourceVm>>> GetCourcesByName([FromBody] GetCourcesByNameQuery query)
    {
        try
        {
            var response = await Mediator.Send(query);
            return new ControllerResponse<IList<CourceVm>>(response);
        }
        catch (Exception e)
        {
            return new ControllerResponse<IList<CourceVm>>(e);
        }
    }
    
    [HttpGet("GetCourcesByTopic")]
    public async Task<IList<CourceVm>> GetCourcesByTopic([FromQuery] GetCourcesByTopicQuery query)
    {
        var cources =  await Mediator.Send(query);
        return cources;
    }
    
    [HttpGet("GetCourceDescription")]
    public async Task<IList<ExpanderVm>> GetCourceDescription([FromQuery] GetCourceDescriptionQuery query)
    {
        var expanders =  await Mediator.Send(query);
        return expanders;
    }
    
    [HttpPost("SetRoomIdToCource")]
    public async Task<bool> SetRoomIdToCource([FromBody] SetRoomIdToCourceQuery query)
    {
        var isModified =  await Mediator.Send(query);
        return isModified;
    }

    [HttpGet("DowloadFileByName")]
    public async Task<FileStreamResult> DowloadFileByName([FromQuery] string fileName)
    {
        var query = new DowloadFileByNameQuery
        {
            FileName = fileName,
        };

        var t = await Mediator.Send(query);
        return File(t, "image/png");
    }
    
    [HttpGet("DowloadFileById")]
    public async Task<FileStreamResult> DowloadFileById([FromQuery] string Id)
    {
        var query = new DowloadFileByIdQuery
        {
            Id = ObjectId.Parse(Id),
        };

        var fileVm = await Mediator.Send(query);
        return File(fileVm.FileStream, fileVm.Type);
    }

    [HttpGet("GetChaptersAndSubjects")]
    public async Task<ChaptersSubjects> GetChaptersAndSubjects([FromQuery] string id)
    {
        var chapterQuery = new GetChaptersByCourceIdQuery { CourceId = id };
        var chaptersSubjects = new ChaptersSubjects();
        var chapters = await Mediator.Send(chapterQuery);
        var cource = await Mediator.Send(new GetCourceByIdQuery { Id = id });
        chaptersSubjects.CourceName = cource.Name ?? "";
        foreach (var chapter in chapters)
        {
            var subjectVms = await Mediator.Send(new GetSubjectsByChapterIdQuery { ChapterId = chapter.Id });
            chaptersSubjects.Values.Add(new ChaptersSubjectsValues {Subjects = subjectVms, Chapter = chapter});
        }

        return chaptersSubjects;
    }
}

public class ChaptersSubjects
{
    public string CourceName { get; set; }
    public List<ChaptersSubjectsValues> Values { get; set; } = new List<ChaptersSubjectsValues>();
}

public class ChaptersSubjectsValues
{
    public ChapterVm Chapter { get; set; }
    public IList<SubjectVm> Subjects { get; set; } = new List<SubjectVm>();
}