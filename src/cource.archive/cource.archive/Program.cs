using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using cource.archive;
using cource.archive.application;
using cource.archive.application.Mapping;
using cource.archive.domain.Settings;
using cource.archive.Services;
/*using Microsoft.AspNetCore.Authentication.Certificate;*/
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.AspNetCore.Server.Kestrel.Https;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

var builder = WebApplication.CreateBuilder(args);

/*builder.Services.AddAuthentication(
        CertificateAuthenticationDefaults.AuthenticationScheme)
    .AddCertificate();*/

/*builder.Services.Configure<KestrelServerOptions>(options =>
{
    options.ConfigureHttpsDefaults(options =>
        options.ClientCertificateMode = ClientCertificateMode.RequireCertificate);
});*/

/*var clientCertificate = X509Certificate2.CreateFromPem(
    File.ReadAllText(
        "/usr/local/share/ca-certificates/Cloudflare_CA.crt"));*/

/*builder.Services.AddHttpClient("courceArchive", c => { })
    .ConfigurePrimaryHttpMessageHandler(() => {
        var handler = new HttpClientHandler();
        handler.ClientCertificates.Add(clientCertificate);
        return handler;
    });*/

builder.Services.AddAutoMapper(config =>
{
    config.AddProfile(new AssemblyMappingProfile(Assembly.Load("cource.archive.application")));
});

// Add services to the container.
builder.Services.AddSingleton<IMongoClient>(s =>
    new MongoClient(builder.Configuration.GetValue<string>("MongoDBSettings:ConnectionString")));
builder.Services.AddControllers();

builder.Services.Configure<MongoDBSettings>(
    builder.Configuration.GetSection(nameof(MongoDBSettings)));

builder.Services.AddSingleton<IMongoDBSettings>(sp =>
    sp.GetRequiredService<IOptions<MongoDBSettings>>().Value);

builder.Services.AddMediatr();

builder.Services.AddSingleton<IFileService, FileService>();


// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddCors();

const string policyName = "CorsPolicy";
builder.Services.AddCors(options =>
{
    options.AddPolicy(name: policyName, builder =>
    {
        builder.AllowAnyOrigin()
            .AllowAnyHeader()
            .AllowAnyMethod()
            .SetIsOriginAllowed((host) => true);
    });
});
/*builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowAll", policy =>
    {
        policy.AllowAnyHeader();
        policy.AllowAnyMethod();
        policy.AllowAnyOrigin();
    });
});*/

/*builder.WebHost.ConfigureKestrel(s =>
{
    s.ListenAnyIP(443, options => { options.UseHttps(clientCertificate); });
});*/

var app = builder.Build();

app.UseSwagger();
app.UseSwaggerUI();

/*app.UseAuthentication();*/
//app.UseHttpsRedirection();

app.UseRouting();
app.UseCors(policyName);
app.UseAuthorization();
app.MapControllers();
app.Run();
