using System.Reflection;
using AutoMapper;
using cource.archive.application.Cource.Queries.GetChapterByCourceId;
using cource.archive.application.Cource.Queries.GetChapterById;
using cource.archive.application.Cource.Queries.GetCourceById;
using cource.archive.application.Cource.Queries.GetCourceDescription;
using cource.archive.application.Cource.Queries.GetCourceList;
using cource.archive.application.Cource.Queries.GetCourcesByTopic;
using cource.archive.application.Cource.Queries.GetSubjectsByChapterId;
using cource.archive.application.Cource.Queries.GetTopicList;
using cource.archive.application.Mapping;
using cource.archive.domain.Settings;
using MongoDB.Driver;

namespace cource.archive.Test
{
    public class UnitTest1
    {
        public MongoDBSettings Settings = new MongoDBSettings
        {
            DataBaseName = "courceschema",
            FileDataBase = "filesshema",
            CourceCollectionName = "cource",
            ChapterCollectionName = "chapter",
            TopicCollection = "topic",
            CourceSpecification = "courceSpecification",
            ExpanderVariant = "expanderVariant",
            ExpanderVariantValue = "expanderVariantValue",
            Subject = "subject"
        };

        public MongoClient Client = new MongoClient("mongodb://localhost:27017");

        public IMapper Mapper = new Mapper(new MapperConfiguration(mc =>
            mc.AddProfile(new AssemblyMappingProfile(Assembly.Load("cource.archive.application")))));

        [Fact]
        public async Task GetTopicsTest()
        {
            try
            {
                //var mediator = new  Mock<IMediator>();
                var command = new GetTopicListQuery();
                var handler = new GetTopicListHandler(Settings, Client, Mapper);

                var result = await handler.Handle(command, new CancellationToken());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Fact]
        public async Task GetCourcesByTopicId()
        {
            try
            {
                var command = new GetCourcesByTopicQuery { TopicId = "661406136533927f3b3b1085" };
                var handler = new GetCourcesByTopicHandler(Settings, Client, Mapper);
                var result = await handler.Handle(command, new CancellationToken());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Fact]
        public async Task GetCourceDescription()
        {
            try
            {
                var command = new GetCourceDescriptionQuery { CourceId = "6619baea3c110da9db08dc04" };
                var handler = new GetCourceDescriptionHandler(Settings, Client, Mapper);

                var result = await handler.Handle(command, new CancellationToken());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Fact]
        public async Task GetCourceList()
        {
            try
            {
                var command = new GetCourceListQuery();
                var handler = new GetCourceListHandler(Settings, Client, Mapper);

                var result = await handler.Handle(command, new CancellationToken());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Fact]
        public async Task GetSubjectsByChapterId()
        {
            try
            {
                var command = new GetSubjectsByChapterIdQuery { ChapterId = "66379c0b2e1aae43a3708dbd" };
                var handler = new GetSubjectsByChapterIdHandler(Settings, Client, Mapper);

                var result = await handler.Handle(command, new CancellationToken());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Fact]
        public async Task GetCourceById()
        {
            try
            {
                var command = new GetCourceByIdQuery { Id = "6619baea3c110da9db08dc04" };
                var handler = new GetCourceByIdHandler(Settings, Client, Mapper);

                var result = await handler.Handle(command, new CancellationToken());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Fact]
        public async Task GetChapterById()
        {
            try
            {
                var command = new GetChapterByIdQuery { Id = "661c1438503320cacb0ea8f7" };
                var handler = new GetChapterByIdHandler(Settings, Client, Mapper);

                var result = await handler.Handle(command, new CancellationToken());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Fact]
        public async Task GetChaptersByCourceId()
        {
            try
            {
                var command = new GetChaptersByCourceIdQuery { CourceId = "6619baea3c110da9db08dc04" };
                var handler = new GetChaptersByCourceIdHandler(Settings, Client, Mapper);

                var result = await handler.Handle(command, new CancellationToken());
            }
            catch (Exception e)
            {
                Assert.Fail(e.Message);
            }
        }

        [Fact]
        public void Test1()
        {

        }
    }
}